#include "execmon_config.h"
#include <unistd.h>
#include <getopt.h>

namespace execmon
{
Config::Config(int argc, char **argv) :
  usage_(std::string("Usage: ") + argv[0]),
  deny_list_file_("apps.deny"),
  daemonize_(true)
{
  usage_.append(": [-n] [-c deny_list_file]");
  usage_.append("\n-n\tdo not perform daemonization");
  usage_.append("\n-c deny_list_file\tuse deny_list_file as config");
  ParseCmd(argc, argv);
}

void Config::ParseCmd(int argc, char **argv)
{
  int opt;
  while ((opt = getopt(argc, argv, "nc:")) != -1)
  {
    switch(opt)
    {
    case 'n':
      daemonize_ = false;
      break;
    case 'c':
      deny_list_file_ = optarg;
      break;
    default: // '?'
      throw ConfigException(usage_);
    }
  }
}

const std::string& Config::DenyListFile() const noexcept
{
  return deny_list_file_;
}

bool Config::Daemonize() const noexcept
{
  return daemonize_;
}

} // namespace execmon
