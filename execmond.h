#ifndef _EXECMOND_H_
#define _EXECMOND_H_
#include <stdexcept>
#include <linux/limits.h>
#include <unordered_set>
#include "execmon_config.h"

namespace execmon
{

class ModuleConnectorException : public std::runtime_error
{
public:
  ModuleConnectorException(const std::string& str)
    : std::runtime_error(str)
  {}

  ModuleConnectorException(const char* str)
    : std::runtime_error(str)
  {}
};

class ModuleConnector
{
public:
  explicit ModuleConnector(const Config& config);
  ~ModuleConnector();

  void Run();

private:
// Set signal handler(s)
void InitSignals() const noexcept;

// Find module major number
void FindMajor();

// Create module device file in /dev/
void CreateDevice();

// Remove module device from /dev
void RemoveDevice();

// Open device file
void OpenDevice();

// Close device descriptor
void CloseDevice();

// Read config file and populate deny list
void PopulateDenyList();

private:
typedef std::unordered_set<std::string> AppsList;

private:
Config config_;
unsigned module_major_;
static const std::string device_name_;
int fd_;
char buf_[PATH_MAX];
const std::string config_file_;
AppsList deny_list_;
};

} // namespace execmon
#endif // _EXECMOND_H_
