#include <iostream>
#include <cstdlib>
#include "execmond.h"

int main(int argc, char **argv)
{
	try
	{
		execmon::ModuleConnector connector(execmon::Config(argc, argv));
		connector.Run();
	}
  catch(const execmon::ConfigException& e)
  {
    std::cerr << "Config error: " << e.what() << std::endl;
    return EXIT_FAILURE;
  }
	catch(const execmon::ModuleConnectorException& e)
	{
		std::cerr << "Connector error: " << e.what() << std::endl;
		return EXIT_FAILURE;
	}
	catch(const std::exception& e)
	{
		std::cerr << "Error: " << e.what() << std::endl;
		return EXIT_FAILURE;
	}
	catch(...)
	{
		std::cerr << "Unknown error" << std::endl;
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
