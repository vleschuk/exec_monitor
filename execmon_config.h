#ifndef _EXECMON_CONFIG_H_
#define _EXECMON_CONFIG_H_
#include <string>
#include <stdexcept>

namespace execmon
{

class ConfigException : public std::runtime_error
{
public:
  ConfigException(const std::string& str)
    : std::runtime_error(str)
  {}

  ConfigException(const char* str)
    : std::runtime_error(str)
  {}
};

class Config
{
public:
  Config(int argc, char **argv);
  const std::string& DenyListFile() const noexcept;
  bool Daemonize() const noexcept;

private:
  void ParseCmd(int argc, char **argv);
private:
  std::string usage_;
  std::string deny_list_file_;
  bool daemonize_;
};
} // namespace execmon
#endif // _EXECMON_CONFIG_H_
