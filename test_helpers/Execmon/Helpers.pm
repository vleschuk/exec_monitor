package Execmon::Helpers;
use POSIX ":sys_wait_h";
use threads;

sub RunCmd
{
  my $cmd = shift;
  my $pid = fork();
  if ($pid == 0) # child
  {
    exec $cmd or exit 1;
  }
  elsif ($pid > 0) # parent
  {
    waitpid($pid, 0);
    return $res = $? >> 8;
  }

  # error
  return 2;
}

sub CheckRunCmd
{
  my $fh = shift;
  my $cmd = shift;
  my $action = shift;
  my $do_not_answer = shift;
  my $thr = threads->create('Execmon::Helpers::RunCmd', $cmd);
  while(1)
  {
    my $in = '';
    vec($in, fileno($fh), 1) = 1;
    select($in, undef, undef, undef);
    my $str = <$fh>;
    next unless $str;
    last if "$str" eq "$cmd";
  }
  unless($do_not_answer)
  {
    syswrite $fh, $action, 1 or print "Failed to write to device: $!\n";
  }
  my $res = $thr->join();
  return $res;
}

1;
