MODULE_SUBDIR = monitor_module
INCLUDE = -I$(MODULE_SUBDIR) -I.
CXXFLAGS = -std=c++0x -Wall -Wextra -Werror -pedantic $(INCLUDE)

all: execmond execmon_module

execmon_module:
	cd $(MODULE_SUBDIR) && $(MAKE)

execmon_config.o: execmon_config.cpp
	$(CXX) -c $(CXXFLAGS) execmon_config.cpp -o $@

execmond.o: execmond.cpp
	$(CXX) -c $(CXXFLAGS) execmond.cpp -o $@

execmond: execmon_config.o execmond.o main.cpp
	$(CXX) $(CXXFLAGS) execmon_config.o execmond.o main.cpp -o $@

clean:
	rm -f *.o execmond
	cd $(MODULE_SUBDIR) && $(MAKE) clean
