# README #

This is an process startup control application. It consists of userspace daemon (execmond) and a kernel module (execmon). One can put list of application he doesn't want to be run into configuration file (example: apps.deny). Load the module and start the daemon, after that nobody will be able to run any of listed applications.

# BUILD #
From the project directory issue the following command:

*$ make*

# RUNNING #
**You must be root to do the following**

Load the module

*# insmod ./monitor_module/execmon.ko*

Run the daemon

*# ./execmond*

execmond supports the following command line options

* -c /path/to/configuration/file - manually specify the list of denied applications file (default: apps.deny)

* -n - do not daemonize (default: false)

# TESTING #
For testing you will need perl installed and the following packages: POSIX, Fcntl, Test::More (all of them usually are in base perl package). 

*$ sudo mknod -m 0666 /dev/execmon c <major> 0*

where <major> is major number of device. You can find it by 'grep execmon /proc/devices'.

After that just run the following command:

*$ prove*

# Supported platforms #

Tested on Ubuntu 12.04 LTS (i386), Ubuntu 14.10 (x86_64), CentOS 7(x86_64).