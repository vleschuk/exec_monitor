#!/usr/bin/env perl
use FindBin qw/ $Bin /;
use lib "$Bin/../test_helpers";
use Execmon::Helpers qw/ CheckRunCmd /;
use Test::More qw/ no_plan /;
use Fcntl;

use constant DEVICE => "/dev/execmon";

use strict;
use warnings;

BEGIN
{
  die "Device " . DEVICE . " doesn't exist.\n"
      ."Create it with 'sudo mknod -m 0666 " . DEVICE . " c <major> 0"
  unless -e DEVICE;
}

sysopen my $fh, DEVICE, O_RDWR or die "Can't open " . DEVICE . ": $!";

ok(1 == Execmon::Helpers::CheckRunCmd($fh, "/bin/echo", 'd'), "Failed to run denied application");

ok(0 == Execmon::Helpers::CheckRunCmd($fh, "/bin/echo", 'a'), "Allowed application runs successfully");

ok(0 == Execmon::Helpers::CheckRunCmd($fh, "/bin/echo", 'e'), "Application with invalid command runs successfully");

ok(0 == Execmon::Helpers::CheckRunCmd($fh, "/bin/echo", undef), "Application with empty command runs successfully");

ok(0 == Execmon::Helpers::CheckRunCmd($fh, "/bin/echo", 'd', 1), "Application with no reply runs successfully");

close $fh;
