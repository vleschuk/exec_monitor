/*
 * exec_monitor.c
 *
 * Copyright (C) Victor Leschuk <vleschuk@gmail.com>
 *
 * Driver creating pseudo-device for controlling applications startup
 * It controls exec() syscalls and asks userspace program
 * whether it is allowed to run this app or not
 *
 * The name of application being run is passed to runtime via read() syscall
 * The answer should be passed back via write()
 * Notifications about applications being run can be obtained via select()/poll()/epoll()
 *
 * NOTE: only one userspace application can use this driver at once (e.g. only 1 open() is allowed)
 */


#define pr_fmt(fmt) KBUILD_MODNAME ": " fmt
#include <linux/printk.h>
#include <linux/module.h>
#include <linux/binfmts.h>
#include <linux/cdev.h>
#include <linux/limits.h>
#include <linux/poll.h>
#include <linux/sched.h>
#include <asm/uaccess.h>
#include <asm/atomic.h>
#include <asm/spinlock.h>
#include "probe.h"
#include "exec_monitor.h"

static int execmon_major = 0;
static int execmon_minor = EXECMON_MINOR;

DECLARE_WAIT_QUEUE_HEAD(write_wq);
static char action_state = EXECMON_ERROR; 
static atomic_t open_count = ATOMIC_INIT(-1);

DECLARE_WAIT_QUEUE_HEAD(read_wq);
static rwlock_t cmd_lock;
static char current_cmd[PATH_MAX];
static size_t cmd_len = 0;

static int execmon_open(struct inode *inode, struct file *filp);
static int execmon_release(struct inode *inode, struct file *filp);
static ssize_t execmon_read(struct file *filp, char *buf, size_t count, loff_t *ppos);
static ssize_t execmon_write(struct file *filp, const char *buf, size_t count, loff_t *offp);
static unsigned int execmon_poll(struct file *filp, poll_table *wait);

static struct file_operations execmon_fops =
{
  .owner = THIS_MODULE,
  .open = execmon_open,
  .release = execmon_release,
  .read = execmon_read,
  .write = execmon_write,
  .poll = execmon_poll,
};

static struct security_operations *ops = NULL;

/* In modern kernels register_security() function is not exported
 * so we shall need to probe for current security_operations structure
 * and replace its bprm_check_security() pointer.
 * Of course we need to save the old pointer.
 */
static int(*old_bprm_check_security)(struct linux_binprm *bprm) = NULL;

static struct cdev *execmon_cdev = NULL;

static int exec_hook(struct linux_binprm *bprm)
{
  size_t len;
  int res;
  pr_debug("exec hooked. filename: %s\n", bprm->filename);

  if (-1 == atomic_read(&open_count))
  {
    pr_debug("No one is connected to module, skipping checks");
    goto orig;
  }

  len = strlen(bprm->filename);
  pr_debug("Writing cmd to buf\n");
  write_lock(&cmd_lock);
  strncpy(current_cmd, bprm->filename, len);
  cmd_len = len;
  action_state = EXECMON_WAIT;
  write_unlock(&cmd_lock);
  pr_debug("cmd written. Waking up read\n");
  wake_up_interruptible(&read_wq);

  res = wait_event_interruptible_timeout(write_wq, action_state != EXECMON_WAIT, msecs_to_jiffies(EXECMON_TIMEOUT));
  if (0 == res) /* timeout */
  {
    pr_debug("Timeout waiting for user response for command: %s\n", bprm->filename);
    action_state = EXECMON_ERROR;
    goto orig;
  }

  switch(action_state)
  {
  case EXECMON_WAIT:
    action_state = EXECMON_ERROR;
  case EXECMON_ERROR:
    break;
  case EXECMON_ALLOW:
    pr_debug("User allowed command: %s\n", bprm->filename);
    goto orig;
  case EXECMON_DENY:
    pr_debug("User denied command: %s\n", bprm->filename);
    return -EACCES;
  default:
    pr_err("Invalid response from user: %c\n", action_state);
    break;
  }

orig:
  return old_bprm_check_security(bprm);
}

static int execmon_open(struct inode *inode, struct file *filp)
{
  if (atomic_inc_and_test(&open_count))
  {
    /* counter increased and became 0, we are the first client */
    pr_debug("Device opened by user\n");
    return 0;
  }

  pr_err("Somebody is already talking to module\n");
  atomic_dec(&open_count);
  return -EACCES;
}

static int execmon_release(struct inode *inode, struct file *filp)
{
  atomic_dec(&open_count);
  pr_debug("Device closed by user\n");
  return 0;
}

static ssize_t execmon_read(struct file *filp, char *buf, size_t count, loff_t *ppos)
{
  size_t len;
  ssize_t res;
  pr_debug("Read called\n");
  read_lock(&cmd_lock);
  len = cmd_len;
  if (count < len)
  {
    pr_err("count %zu is less than len %zu\n", count, len);
    res = -EINVAL;
    goto cleanup;
  }

  if (copy_to_user(buf, current_cmd, len))
  {
    pr_err("Failed to copy to user\n");
    res = -EFAULT;
    goto cleanup;
  }

  *ppos = len;
  res = len;
cleanup:
  cmd_len = 0; /* let's say all data was read */
  read_unlock(&cmd_lock);
  return res;
}

static ssize_t execmon_write(struct file *filp, const char *buf, size_t count, loff_t *offp)
{
  if (action_state != EXECMON_WAIT)
  {
    pr_err("Can't write to device: state is '%c'\n", action_state);
    return -EPERM;
  }

  if (count != 1)
  {
    return -EINVAL;
  }

  if (get_user(action_state, buf))
  {
    pr_err("Failed to read from user\n");
    return -EFAULT;
  }

  pr_debug("Received reply: %c\n", action_state);

  wake_up_interruptible(&write_wq);
  return count;
}

static unsigned int execmon_poll(struct file *filp, poll_table *wait)
{
  pr_debug("Polling for read_wq\n");
  poll_wait(filp, &read_wq, wait);
  pr_debug("Poll finished\n");
  return POLLIN | POLLRDNORM;
}

static int execmon_register_device(void)
{
  dev_t dev = 0;
  int res;
  int devno;

  res = alloc_chrdev_region(&dev, execmon_minor, 1, EXECMON_DEV_NAME);
  if (res < 0)
  {
    pr_err("Failed to obtain device major number: %d\n", res);
    goto cleanup;
  }

  execmon_major = MAJOR(dev);

  devno = MKDEV(execmon_major, execmon_minor);

  execmon_cdev = cdev_alloc();
  if(!execmon_cdev)
  {
    pr_err("Failed to allocate cdev structure\n");
    res = -1;
    goto cleanup;
  }
  execmon_cdev->owner = THIS_MODULE;
  execmon_cdev->ops = &execmon_fops;

  res = cdev_add(execmon_cdev, dev, 1);
  if (res < 0)
  {
    pr_err("Failed to register device: %d\n", res);
    goto cleanup;
  }

  pr_info("Device registered successfully. major: %d minor: %d", execmon_major, execmon_minor);

cleanup:
  return res;
}

static int __init execmon_init(void)
{
  int ret = 0;

  rwlock_init(&cmd_lock);

  ret = execmon_register_device();
  if(ret < 0)
  {
    pr_err("Failed to register device");
    goto cleanup;
  }

	ops = probe_security_ops();
	if (!ops)
  {
    pr_err("Failed to find security_ops struct\n");
    ret = -1;
    goto cleanup;
  }
  pr_debug("Successfully probed security_ops struct\n");

  old_bprm_check_security = ops->bprm_check_security;
  smp_mb();
  ops->bprm_check_security = exec_hook;

cleanup:
  return ret;
}

static void execmon_exit(void)
{
  dev_t dev = MKDEV(execmon_major, execmon_minor);
  unregister_chrdev_region(dev, 1);

  if(ops)
  {
    ops->bprm_check_security = old_bprm_check_security;
  }
  if(execmon_cdev)
  {
    cdev_del(execmon_cdev);
  }
  pr_info("Exiting\n");
}

module_init(execmon_init);
module_exit(execmon_exit);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Victor Leschuk <vleschuk@gmail.com>");
