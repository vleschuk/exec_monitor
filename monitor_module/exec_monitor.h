#ifndef _EXEC_MONITOR_H_
#define _EXEC_MONITOR_H_

/* Device minor number */
#define EXECMON_MINOR 0

/* Device name */
#define EXECMON_DEV_NAME "execmon"

/* Constants defining current state of driver */

#define EXECMON_WAIT 'w' /* we sent data to user, waiting for reply */
#define EXECMON_ERROR 'e' /* error state */
#define EXECMON_ALLOW 'a' /* allow exec (reply from user) */
#define EXECMON_DENY 'd' /* deny exec (reply from user) */

/* Timeout for waiting user response */
#define EXECMON_TIMEOUT 1000 /* msecs */

#endif /* _EXEC_MONITOR_H_ */
