#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <cstring>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/select.h>
#include <fcntl.h>
#include <signal.h>

#include "execmond.h"
#include "exec_monitor.h"

namespace
{
sig_atomic_t stop_signal_received = -1;
void HandleSignal(int signum)
{
  const char msg[] = "Stop signal received\n";
  stop_signal_received = signum;
  write(2, static_cast<const void *>(msg), sizeof(msg));
}
} // anonymous namespace

namespace execmon
{

const std::string ModuleConnector::device_name_ = std::string("/dev/") + EXECMON_DEV_NAME;

ModuleConnector::ModuleConnector(const Config& config) :
  config_(config),
  module_major_(-1),
  fd_(-1)
{
}

ModuleConnector::~ModuleConnector()
{
  CloseDevice();
  RemoveDevice();
}

void ModuleConnector::InitSignals() const noexcept
{
  /*
  sigset_t set;
  sigfillset(&set);
  sigprocmask(SIG_SETMASK, &set, 0);
  */

  struct sigaction act;
  memset(static_cast<void *>(&act), 0, sizeof(act));
  //sigfillset(&act.sa_mask);
  act.sa_handler = HandleSignal;
  act.sa_flags = SA_RESTART;

  sigaction(SIGINT, &act, 0);
  sigaction(SIGTERM, &act, 0);
#if defined(SIGQUIT)
  sigaction(SIGQUIT, &act, 0);
#endif // defined(SIGQUIT)
}

void ModuleConnector::PopulateDenyList()
{
  std::ifstream config(config_.DenyListFile());

  if (!config.is_open())
  {
    std::ostringstream ss;
    ss << "Failed to open file " << config << " for reading: " << strerror(errno);
    throw ModuleConnectorException(ss.str());
  }

  std::string line;
  while (getline(config, line))
  {
    if (line.empty())
    {
      continue;
    }

    deny_list_.insert(line);
  }
  config.close();

  std::cout << "Read " << deny_list_.size() << " entries into deny list" << std::endl;
}

void ModuleConnector::FindMajor()
{
  static const char *dev_filename = "/proc/devices";

  std::ifstream dev_file(dev_filename);

  if (!dev_file.is_open())
  {
    std::ostringstream ss;
    ss << "Failed to open file " << dev_filename << " for reading: " << strerror(errno);
    throw ModuleConnectorException(ss.str());
  }

  std::string line;
  while (getline(dev_file, line))
  {
    if (line.empty())
    {
      continue;
    }

    const auto pos = line.find(EXECMON_DEV_NAME);
    if (std::string::npos == pos)
    {
      continue;
    }

    // found
    module_major_ = std::stoi(line.substr(0, pos));
  }
  dev_file.close();
}

void ModuleConnector::CreateDevice()
{
  if (-1 == mknod(device_name_.c_str(), S_IFCHR|0600, makedev(module_major_, EXECMON_MINOR)) && errno != EEXIST)
  {
    std::ostringstream ss;
    ss << "Failed to create device " << device_name_ << ": " << strerror(errno);
    throw ModuleConnectorException(ss.str());
  }
}

void ModuleConnector::RemoveDevice()
{
  if (unlink(device_name_.c_str()))
  {
    std::cerr << "Failed to remove " << device_name_ << ": " << strerror(errno) << std::endl;
  }
}

void ModuleConnector::OpenDevice()
{
  if ( -1 == (fd_ = open(device_name_.c_str(), O_RDWR)))
  {
    std::ostringstream ss;
    ss << "Failed to open " << device_name_ << ": " << strerror(errno);
    throw ModuleConnectorException(ss.str());
  }
  std::cout << "Device opened successfully" << std::endl;
}

void ModuleConnector::CloseDevice()
{
  if (-1 == fd_)
  {
    return;
  }

  if (-1 == close(fd_))
  {
    std::cerr << "Failed to close device descriptor: " << strerror(errno) << std::endl;
  }
}

void ModuleConnector::Run()
{
  PopulateDenyList();
  FindMajor();
  if (static_cast<unsigned>(-1) == module_major_)
  {
    throw ModuleConnectorException("Can't find module major number");
  }
  std::cout << "Module device major number is: " << module_major_ << std::endl;

  if (config_.Daemonize())
  {
    if (-1 == daemon(0, 0))
    {
      std::ostringstream ss;
      ss << "Failed to daemonize: " << strerror(errno);
      throw ModuleConnectorException(ss.str());
    }
  }

  InitSignals();

  CreateDevice();
  OpenDevice();

  while (true)
  {
    if (-1 != stop_signal_received)
    {
      break;
    }

    fd_set read_set;
    FD_ZERO(&read_set);
    FD_SET(fd_, &read_set);
    int fds;
    if ( -1 == (fds = select(fd_ + 1, &read_set, 0, 0, 0)))
    {
      std::ostringstream ss;
      ss << "Select failed: " << strerror(errno);
      throw ModuleConnectorException(ss.str());
    }

    if (!FD_ISSET(fd_, &read_set))
    {
      std::ostringstream ss;
      ss << "Select finished, but our descriptor is not ready!";
      throw ModuleConnectorException(ss.str());
    }

    ssize_t bytes = read(fd_, static_cast<void *>(buf_), sizeof(buf_));
    if (bytes < 0)
    {
      std::ostringstream ss;
      ss << "Read error: " << strerror(errno);
      throw ModuleConnectorException(ss.str());
    }
    if (0 == bytes)
    {
      continue;
    }
    std::cout << "Read " << bytes << " bytes from the device" << std::endl;
    buf_[bytes] = '\0';
    std::cout << "The string read: " << buf_ << std::endl;

    const auto it = deny_list_.find(std::string(buf_));
    char answer = (it == deny_list_.end()) ? EXECMON_ALLOW : EXECMON_DENY;
    if (-1 == write(fd_, &answer, 1))
    {
      std::cerr << "Failed to write to device: " << strerror(errno) << std::endl;
    }
  }

}

} // namespace execmon
